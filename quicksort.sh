# create result folder to store the loggings
#mkdir ResultPI-quicksort

# compile the program
javac -cp /eecs/home/qiyitang/experiment/BFSDistanceOne/commons-math3-3.4.1.jar Bisimilarity.java Pair.java SplayTree.java  Simple.java General.java MeasureTime.java

# measure time
#java -Xmx13g -cp .:/eecs/home/qiyitang/experiment/BFSDistanceOne/commons-math3-3.4.1.jar MeasureTime quicksort6 0.0000001 1 ResultPI-quicksort/q6-simple ResultPI-quicksort/q6-general &>ResultPI-quicksort/quick6-print
#java -Xmx13g -classpath .:~/experiment/BFSDistanceOne/ MeasureTime quicksort6 0.0000001 1 ResultPI-quicksort/q6-simple ResultPI-quicksort/q6-general
#java -Xmx13g -classpath .:~/experiment/BFSDistanceOne/commons-math3-3.4.1.jar MeasureTime quicksort5 0.0000001 1 ResultPI-quicksort/q5-simple ResultPI-quicksort/q5-general
#java -Xmx13g -classpath .:/eecs/home/qiyitang/experiment/BFSDistanceOne/commons-math3-3.4.1.jar MeasureTime \
#     quicksort5 0.0000001 1 ResultPI-quicksort/q5-simple ResultPI-quicksort/q5-general > ResultPI-quicksort/q5-print
java -Xmx13g -classpath .:/eecs/home/qiyitang/experiment/BFSDistanceOne/commons-math3-3.4.1.jar MeasureTime \
     quicksort4 0.0000001 1 ResultPI-quicksort/q4-simple ResultPI-quicksort/q4-general > ResultPI-quicksort/q4-print
