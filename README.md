Probabilistic bisimilarity distances
------------------------------------

An introduction to probabilistic bisimilarity distances for labelled Markov chains can be found in [1].  Algorithms to compute these probabilistic bisimilarity distances can be found in [2,3,4].  Here, we provide implementations of those algorithms in Java.

[1] Franck van Breugel. Probabilistic bisimilarity distances. SIGLOG News 4(4): 33-51 (2017)  
[2] Qiyi Tang and Franck van Breugel. Computing Probabilistic Bisimilarity Distances via Policy Iteration. CONCUR 2016: 22:1-22:15  
[3] Qiyi Tang and Franck van Breugel. Algorithms to Compute Probabilistic Bisimilarity Distances for Labelled Markov Chains. CONCUR 2017: 27:1-27:16  
[4] Qiyi Tang, Franck van Breugel. Deciding Probabilistic Bisimilarity Distance One for Labelled Markov Chains. CAV (1) 2018: 681-699  

Licensing
---------

This code is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This extension is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You can find a copy of the GNU General Public License at http://www.gnu.org/licenses
