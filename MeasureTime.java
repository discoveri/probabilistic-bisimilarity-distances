import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.*;



public class MeasureTime{


	public static void main(String[] args) {
		if (args.length != 5) {
			System.out.println("Use java MeasureTime <inputFile> <accuracy> <discountFactor> "
					+ "<simpleOut> <generalOut>");
		} else {
			// process the command line arguments
			Scanner input = null;
			try {
				input = new Scanner(new File(args[0]));
			} catch (FileNotFoundException e) {
				System.out.printf("Input file %s not found%n", args[0]);
				System.exit(1);
			}

			try {
				Simple.accuracy = Double.parseDouble(args[1]);
				General.accuracy = Double.parseDouble(args[1]);
	
				assert Simple.accuracy <= 1 : String.format("Accuracy %f is greater than 1", Simple.accuracy);
				assert Simple.accuracy > 0 : String.format("Accuracy %f is smaller than or equal to 0", Simple.accuracy);
			} catch (NumberFormatException e) {
				System.out.println("Accuracy not provided in the right format");
				System.exit(1);
			}

			try {
				Simple.discount = Double.parseDouble(args[2]);
				General.discount = Double.parseDouble(args[2]);
				assert Simple.discount <= 1 : String.format("Discount factor %f is greater than or equal to 1",
						Simple.discount);
				assert Simple.discount > 0 : String.format("Discount factor %f is smaller than or equal to 0",
						Simple.discount);
			} catch (NumberFormatException e) {
				System.out.println("Discount factor not provided in the right format");
				System.exit(1);
			}

			PrintStream simple = null;
			;
			try {
				simple = new PrintStream(new File(args[3]));
			} catch (FileNotFoundException e) {
				System.out.printf("Output file %s not created%n", args[3]);
				System.exit(1);
			}

			PrintStream general = null;
			;
			try {
				general = new PrintStream(new File(args[4]));
			} catch (FileNotFoundException e) {
				System.out.printf("Output file %s not created%n", args[4]);
				System.exit(1);
			}


			// process the input file
			int numOfStates = -1;
			double[] labels = null;
			double[][] tranFunc = null;

			while (input.hasNextInt()) {
				try {
					numOfStates = input.nextInt();
					int numOfTrans = input.nextInt();
					labels = new double[numOfStates];
					for (int i = numOfStates-1; i < numOfStates; i++) {
						labels[i] = i;
						assert labels[i] >= 0 : String.format("Label %f of state %d is smaller than 0", labels[i], i);
						assert labels[i] <= 1 : String.format("Label %f of state %d is greater than 1", labels[i], i);
						System.out.print(labels[i] + " ");
					}
				
					tranFunc = new double[numOfStates][numOfStates];

					for (int i = 0; i < numOfTrans; i++) {
						int row = input.nextInt();
						int col = input.nextInt();
						tranFunc[row][col] += input.nextDouble();
						if(tranFunc[row][col] == 0.33){
							tranFunc[row][col] = 1/3.0;
						}
						if (tranFunc[row][col] == 0.167) {
							tranFunc[row][col] = 1 / 6.0;
						}
					}
					
//					for(int i = 0; i < numOfStates; i++){
//						for (int j =0;j < numOfStates; j++){
//							tranFunc[i][j] = input.nextDouble();
//						}
//					}
//					input.nextLine();
				} catch (NoSuchElementException e) {
					System.out.printf("Input file %s not in the correct format%n", args[0]);
				}
				// simple iteration
				for (int repeat = 0; repeat < 0; repeat++) {
					long start = System.nanoTime();
					Simple comp = new Simple(numOfStates, tranFunc, labels);

					comp.getBisDist();

					simple.println(System.nanoTime() - start);
					// write distances on screen
					String format = "%." + -(int) Math.log10(Simple.accuracy) + "f ";
					for (int i = 0; i < numOfStates; i++) {
						for (int j = 0; j < numOfStates; j++) {
							System.out.printf(format, comp.discrepancy[numOfStates * i + j]);
						}
						System.out.println();
					}
					System.out.println();
				} // end of repeat

                // general iteration
                for (int repeat = 0; repeat < 10; repeat++) {
                    long start = System.nanoTime();
                    General comp = new General(numOfStates, tranFunc, labels);
                    
                    comp.getBisDist();
                    
                    general.println(System.nanoTime() - start);
                    // write distances on screen
                    String format = "%." + -(int) Math.log10(Simple.accuracy) + "f ";
                    for (int i = 0; i < numOfStates; i++) {
                        for (int j = 0; j < numOfStates; j++) {
                            System.out.printf(format, comp.discrepancy[numOfStates * i + j]);
                        }
                        System.out.println();
                    }
                    System.out.println();
                } // end of repeat

            }
		}
	}
}
