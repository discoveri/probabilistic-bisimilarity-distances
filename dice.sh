# create result folder to store the loggings
mkdir Result-Dice

# compile the program
javac -cp /eecs/home/qiyitang/experiment/BFSDistanceOne/commons-math3-3.4.1.jar Bisimilarity.java Pair.java SplayTree.java  Simple.java Simple.java General.java MeasureTime.java 
#javac -cp /eecs/home/qiyitang/experiment/BFSDistanceOne/commons-math3-3.4.1.jar #Bisimilarity.java Pair.java SplayTree.java  SimpleDistanceOne.java GeneralDistanceOne.java #MeasureTime.java

# measure time
java -cp .:/eecs/home/qiyitang/experiment/BFSDistanceOne/commons-math3-3.4.1.jar MeasureTime biased_dice_new 0.0000001 1 Result-Dice/dice-simple Result-Dice/dice-general > Result-Dice/dice-print

