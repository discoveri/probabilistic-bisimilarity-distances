import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.*;

import org.apache.commons.math3.optim.PointValuePair;
import org.apache.commons.math3.optim.linear.LinearConstraint;
import org.apache.commons.math3.optim.linear.LinearConstraintSet;
import org.apache.commons.math3.optim.linear.LinearObjectiveFunction;
import org.apache.commons.math3.optim.linear.NonNegativeConstraint;
import org.apache.commons.math3.optim.linear.Relationship;
import org.apache.commons.math3.optim.linear.SimplexSolver;
import org.apache.commons.math3.optim.nonlinear.scalar.GoalType;

public class General{
	private int numOfStates;
	private double[][] tranFunc;
	private double[] labels;
	public static double discount = 1;
	public static double accuracy = 0.1;
	public double[] discrepancy;
	private Map<Pair, double[]> coupling; 
	private Set<Pair> toCompute = new HashSet<Pair>();
	// private Set<Pair> toCompute;// = new HashSet<Pair>();

	public General(int numOfStates, double[][] tranFunc, double[] labels) {
		this.numOfStates = numOfStates;
		this.tranFunc = tranFunc;
		this.labels = labels;
		this.coupling = new HashMap<>();
	}

	public void getBisDist() {
        long start = System.nanoTime();
		// bisimilarity test
		HashMap<Double, Block> hm = new HashMap<Double, Block>();
		for (int i = 0; i < numOfStates; i++) {
			if (hm.containsKey(labels[i])) {
				State s = new State(i);
				s.block = hm.get(labels[i]);
				hm.get(labels[i]).mList.add(s);
			} else {
				Block b = new Block();
				State s = new State(i);
				s.block = b;
				b.mList.add(s);
				hm.put(labels[i], b);
			}
		}

		LinkedList<Block> partition = new LinkedList<Block>();
		partition.addAll(hm.values());

		Bisimilarity bis = new Bisimilarity();
		bis.lump(partition, tranFunc);

		HashSet<Pair> bisSet = new HashSet<Pair>();

		for (Block b : partition) {
			for (int i = 0; i < b.mList.size(); i++) {
				// if (b.mList.size() >= 1) {
				int xIdx = b.mList.get(i).idx;
				for (int j = i; j < b.mList.size(); j++) {
					int yIdx = b.mList.get(j).idx;
					bisSet.add(new Pair(xIdx, yIdx));
					//System.out.println("[" + xIdx + ", " + yIdx +"]");
				}
				// }
			}
		} // done bisimilar states

		System.out.println("bisimilar size =" + bisSet.size());
        System.out.println("bisimilar time =" + (System.nanoTime() - start));

		this.discrepancy = new double[numOfStates * numOfStates];

		// toCompute set 
		for (int i = 0; i < this.numOfStates; i++) {
			for (int j = 0; j < i; j++) {
				if (this.labels[i] != this.labels[j]) {
					this.discrepancy[i * numOfStates + j] = 1.0;
					this.discrepancy[j * numOfStates + i] = 1.0;
				} else if (bisSet.contains(new Pair(i, j))) {
					this.discrepancy[i * numOfStates + j] = 0;
					this.discrepancy[j * numOfStates + i] = 0;
				} else {
					initialSolution(i, j);
					toCompute.add(new Pair(i, j));
					//System.out.println("[" + i + ", " + j +"]");
				}
			}
		}

		System.out.println("toCompute size =" + toCompute.size());

		// calculate discrepancy
		discrepancySecond();

		boolean loop = false;
		while (!loop) {
			loop = true;
			for (Pair p : toCompute) {
				int s = p.getRow();
				int t = p.getColumn();
				if (!isOptimal(s, t)) {
					loop = false;
					getOptimal(s, t);
					//getOptimal(t, s);
				}
			}
            // calculate discrepancy
            discrepancySecond();
		}

		//System.out.println("toCompute size =" + toCompute.size());
		/*
		for(Pair p: toCompute){
			int s = p.getRow();
			int t = p.getColumn();
			System.out.println(this.discrepancy[s * numOfStates + t]);
		}
		*/
	}

	// TP initial solution North-West Corner method
	public void initialSolution(int s, int t) {
		Pair p = new Pair(s, t);
		double [] arr = new double[this.numOfStates * this.numOfStates];
		

		class Support {
			int state;
			double prob;

			Support(int s, double p) {
				this.state = s;
				this.prob = p;
			}
		}
		Support[] srcTmp = new Support[this.numOfStates];
		for (int i = 0; i < this.numOfStates; i++) {
			srcTmp[i] = new Support(i, tranFunc[s][i]);
		}
		Support[] tgtTmp = new Support[this.numOfStates];
		for (int i = 0; i < this.numOfStates; i++) {
			tgtTmp[i] = new Support(i, tranFunc[t][i]);
		}

		class SupportComparator implements Comparator<Support> {

			@Override
			public int compare(Support o1, Support o2) {
				// TODO Auto-generated method stub
				if (o1.prob == o2.prob)
					return 0;
				else if (o1.prob > o2.prob)
					return -1;
				else
					return 1;
			}

		}

		Arrays.sort(srcTmp, new SupportComparator());
		Arrays.sort(tgtTmp, new SupportComparator());
		
		for (int i = 0; i < this.numOfStates; i++) {
			for (int j = 0; j < this.numOfStates; j++) {
				double min = Math.min(srcTmp[i].prob, tgtTmp[j].prob);
				//System.out.println("prob: " + srcTmp[i].prob + " " + tgtTmp[j].prob + " ");
				int u = srcTmp[i].state;
				int v = tgtTmp[j].state;
				//System.out.println("state: " + u + " " + v + " ");

				arr[u*this.numOfStates + v] = min;
				//arr[v*this.numOfStates + u] = min;
				srcTmp[i].prob = srcTmp[i].prob - min;
				tgtTmp[j].prob = tgtTmp[j].prob - min;
			}
		} // end of for
		coupling.put(p, arr);
	}


	public void discrepancySecond() {
		double epsilon = 0;
		do {
			double[] newDistance = Arrays.copyOf(this.discrepancy, discrepancy.length);
			epsilon = 0;
			for (int i = 0; i < numOfStates; i++) {
				for (int j = 0; j < i; j++) {
					Pair p = new Pair(i, j);
					if (toCompute.contains(p)) {
						this.discrepancy[i * numOfStates + j] = 0;
						double[] arr =coupling.get(p);
						for (int u = 0; u < numOfStates; u++) {
							for (int v = 0; v < numOfStates; v++) {
								double coup = coupling.get(p)[u*this.numOfStates + v];
								//System.out.println("Pair = " + p +"; coulping = " + coup);
								
								this.discrepancy[i * numOfStates + j] += newDistance[u * numOfStates + v]
										* coup;
							}
						}
						this.discrepancy[i * numOfStates + j] = General.discount * this.discrepancy[i * numOfStates + j];
						this.discrepancy[j * numOfStates + i] = this.discrepancy[i * numOfStates + j];
					} // end of if
					epsilon = Math.max(epsilon,
							Math.abs(this.discrepancy[i * numOfStates + j] - newDistance[i * numOfStates + j]));
				}
			} // end of for i j
		} while (epsilon > General.accuracy*0.1);// end of do-while
	}// end of second

	public boolean isOptimal(int s, int t) {
		Pair p = new Pair(s, t);
		double[] solution = getSolution(s, t);
		for (int i = 0; i < this.numOfStates; i++) {
			for (int j = 0; j < this.numOfStates; j++) {
				Double data = solution[i * this.numOfStates + j];
				if (Math.abs(this.coupling.get(p)[i * this.numOfStates + j] - data) > General.accuracy)
					return false;
			}
		}
		return true;
	}

	private double[] getSolution(int s, int t) {
		double[] solution = new double[this.numOfStates * this.numOfStates];
		double[] coeff = Arrays.copyOf(this.discrepancy, this.discrepancy.length);
		LinearObjectiveFunction objective = new LinearObjectiveFunction(coeff, 0);
		Collection<LinearConstraint> constraints = new ArrayList<LinearConstraint>();

		for (int i = 0; i < this.numOfStates; i++) {
			double[] A = new double[this.numOfStates * this.numOfStates];
			Arrays.fill(A, 0);
			for (int j = 0; j < this.numOfStates; j++) {
				A[i * this.numOfStates + j] = 1;
			}
			constraints.add(new LinearConstraint(A, Relationship.EQ, tranFunc[s][i]));
		}

		for (int j = 0; j < this.numOfStates; j++) {
			double[] A = new double[this.numOfStates * this.numOfStates];
			Arrays.fill(A, 0.0);
			for (int i = 0; i < this.numOfStates; i++) {
				A[i * this.numOfStates + j] = 1;
			}
			constraints.add(new LinearConstraint(A, Relationship.EQ, tranFunc[t][j]));
		}

		SimplexSolver solver = new SimplexSolver();
		try{
			PointValuePair optSolution = solver.optimize(objective, new LinearConstraintSet(constraints), GoalType.MINIMIZE,
					new NonNegativeConstraint(true));
			solution = optSolution.getPoint();
			return solution;
		}catch(org.apache.commons.math3.optim.linear.NoFeasibleSolutionException e){
			for(LinearConstraint l: constraints){
				
				for( Double d: l.getCoefficients().toArray()){
					//System.out.print(d + " ");				
				}
				//System.out.println();				
			}	
			return null;
		}


	}

	public void getOptimal(int s, int t) {
		double[] solution = getSolution(s, t);
		Pair p = new Pair(s, t);
		this.coupling.put(p, solution);
	}

}

